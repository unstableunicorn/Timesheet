# -*- mode: python -*-

block_cipher = None

a = Analysis(['timesheet.py'],
             pathex=['C:\\Users\\elric.hindy\\PycharmProjects\\NStimesheets'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='timerecorder',
          debug=False,
          strip=False,
          upx=False,
          console=False , icon='resources\\clock-light-theme.ico')