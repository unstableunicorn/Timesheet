# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'timeentryview.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_timeentryview(object):
    def setupUi(self, timeentryview):
        timeentryview.setObjectName(_fromUtf8("timeentryview"))
        timeentryview.resize(628, 284)
        self.gridLayout = QtGui.QGridLayout(timeentryview)
        self.gridLayout.setContentsMargins(-1, -1, 9, -1)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setContentsMargins(-1, 0, -1, 0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setContentsMargins(-1, 10, -1, -1)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 1, 1, 1, 1)
        self.timeSelectBox = QtGui.QComboBox(timeentryview)
        self.timeSelectBox.setObjectName(_fromUtf8("timeSelectBox"))
        self.gridLayout_2.addWidget(self.timeSelectBox, 0, 0, 1, 1)
        self.dateSelectFrame = QtGui.QFrame(timeentryview)
        self.dateSelectFrame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.dateSelectFrame.setFrameShadow(QtGui.QFrame.Raised)
        self.dateSelectFrame.setObjectName(_fromUtf8("dateSelectFrame"))
        self.gridLayout_3 = QtGui.QGridLayout(self.dateSelectFrame)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.label_2 = QtGui.QLabel(self.dateSelectFrame)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout_3.addWidget(self.label_2, 0, 1, 1, 1)
        self.label = QtGui.QLabel(self.dateSelectFrame)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_3.addWidget(self.label, 0, 0, 1, 1)
        self.dateStartEdit = QtGui.QDateEdit(self.dateSelectFrame)
        self.dateStartEdit.setCalendarPopup(True)
        self.dateStartEdit.setObjectName(_fromUtf8("dateStartEdit"))
        self.gridLayout_3.addWidget(self.dateStartEdit, 1, 0, 1, 1)
        self.dateEndEdit = QtGui.QDateEdit(self.dateSelectFrame)
        self.dateEndEdit.setCalendarPopup(True)
        self.dateEndEdit.setObjectName(_fromUtf8("dateEndEdit"))
        self.gridLayout_3.addWidget(self.dateEndEdit, 1, 1, 1, 1)
        self.gridLayout_2.addWidget(self.dateSelectFrame, 1, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.regexEdit = QtGui.QLineEdit(timeentryview)
        self.regexEdit.setMinimumSize(QtCore.QSize(200, 0))
        self.regexEdit.setObjectName(_fromUtf8("regexEdit"))
        self.horizontalLayout.addWidget(self.regexEdit)
        self.regexButton = QtGui.QPushButton(timeentryview)
        self.regexButton.setObjectName(_fromUtf8("regexButton"))
        self.horizontalLayout.addWidget(self.regexButton)
        self.gridLayout_2.addLayout(self.horizontalLayout, 0, 1, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout_2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(0, -1, 0, 0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.tableView = QtGui.QTableView(timeentryview)
        self.tableView.setObjectName(_fromUtf8("tableView"))
        self.horizontalLayout_3.addWidget(self.tableView)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setContentsMargins(0, -1, -1, -1)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.exportButton = QtGui.QPushButton(timeentryview)
        self.exportButton.setObjectName(_fromUtf8("exportButton"))
        self.verticalLayout.addWidget(self.exportButton)
        self.saveButton = QtGui.QPushButton(timeentryview)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.verticalLayout.addWidget(self.saveButton)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.closeButton = QtGui.QPushButton(timeentryview)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.verticalLayout.addWidget(self.closeButton)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.gridLayout.addLayout(self.verticalLayout_2, 2, 0, 2, 1)

        self.retranslateUi(timeentryview)
        QtCore.QMetaObject.connectSlotsByName(timeentryview)

    def retranslateUi(self, timeentryview):
        timeentryview.setWindowTitle(_translate("timeentryview", "Time Entry View / Export", None))
        self.label_2.setText(_translate("timeentryview", "To:", None))
        self.label.setText(_translate("timeentryview", "From:", None))
        self.dateStartEdit.setDisplayFormat(_translate("timeentryview", "dd/MM/yyyy", None))
        self.dateEndEdit.setDisplayFormat(_translate("timeentryview", "dd/MM/yyyy", None))
        self.regexButton.setText(_translate("timeentryview", "Find", None))
        self.exportButton.setToolTip(_translate("timeentryview", "Export to CSV", None))
        self.exportButton.setText(_translate("timeentryview", "Export", None))
        self.saveButton.setStatusTip(_translate("timeentryview", "Save Changes to Time Sheet", None))
        self.saveButton.setText(_translate("timeentryview", "Save", None))
        self.closeButton.setText(_translate("timeentryview", "Close", None))

