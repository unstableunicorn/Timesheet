import csv
import datetime
import logging
import os
import shutil
import subprocess
import sys
import time
import traceback
import types
import xml.etree.ElementTree as Etree
from distutils.version import StrictVersion
from xml.dom import minidom
from xml.etree.ElementTree import ElementTree, Element

from PyQt4 import QtGui, QtCore

import addtaskwindow
import timeentryview
import timelogoption
import timerecorderupdater as updater
import timerecordmain

__version__ = "2.0.6"

# The following makes sure the icon is correctly shown in the task bar for windows
# Otherwise will show as a python or QT icon.
if sys.platform == 'win32':
    import ctypes

    myappid = u'seeingmachines.recordtimeentry.0.1'  # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
elif sys.platform == 'linux2':
    WindowsError = None


class Config(object):
    """empty class for holding settings"""
    def load_etree(self, root):
        def get_attr(parent, attr):
            for key in attr:
                setattr(parent,key, attr[key])


class CustomQCompleter(QtGui.QCompleter):
    """Customer completer to search full string instead of just the beginning string"""
    def __init__(self, parent=None):
        super(CustomQCompleter, self).__init__(parent)
        self.local_completion_prefix = ""
        self.source_model = None

    def setModel(self, model):
        self.source_model = model
        super(CustomQCompleter, self).setModel(self.source_model)

    def updateModel(self):
        local_completion_prefix = self.local_completion_prefix

        class InnerProxyModel(QtGui.QSortFilterProxyModel):
            def filterAcceptsRow(self, sourceRow, sourceParent):
                index0 = self.sourceModel().index(sourceRow, 0, sourceParent)
                searchStr = local_completion_prefix.lower()
                modelStr = self.sourceModel().data(index0, QtCore.Qt.DisplayRole).toString().toLower()
                # print searchStr,' in ',modelStr, searchStr in modelStr
                return searchStr in modelStr

        proxy_model = InnerProxyModel()

        proxy_model.setSourceModel(self.source_model)

        super(CustomQCompleter, self).setModel(proxy_model)
        # print 'match :',proxy_model.rowCount()
        # self.complete(QtCore.QRect(QtCore.QPoint(-1, -1), QtCore.QSize(1, 1)))

    def splitPath(self, path):
        self.local_completion_prefix = str(path)
        self.updateModel()
        return ""


def unhandled_exception(exc_type, exc_value, exc_tb):
    logger = logging.getLogger(sys._getframe().f_code.co_name)
    logger.error("Unhandled Exception", exc_info=(exc_type, exc_value, exc_tb))
    traceback.print_exception(exc_type, exc_value, exc_tb, limit=10, file=sys.stdout)
    sys.exit(1)


def toprettyxml(root):
    """Takes a Elementtree Obj and returns a prettified xml string"""
    logger = logging.getLogger(sys._getframe().f_code.co_name)
    logger.debug("Generating prettyxml for {}".format(root.tag))
    rough_string = Etree.tostring(root, 'utf-8')
    reparsed_string = minidom.parseString(rough_string).toxml()
    prettyxml = '\n'.join(
        [line for line in minidom.parseString(reparsed_string).toprettyxml(indent=' ' * 2).split('\n') if
         line.strip()])
    return prettyxml


class UpdateThread(QtCore.QThread):
    exitmainapp = QtCore.pyqtSignal()
    """Thread to run an update process in the background"""

    def __init__(self):
        self.__logger = logging.getLogger("{}".format(self.__class__.__name__))
        QtCore.QThread.__init__(self)
        self.__process = None

    def run(self):
        self.__logger.debug("Thread Started")
        # timer for killing the update process
        self.check_update()
        self.__logger.debug("Thread Finished")

    def kill_process(self):
        """function to kill a process"""
        # todo, this does work
        self.__logger.debug("Killing process")
        try:
            self.__process.terminate()
        except Exception as e:
            self.__logger.warning("Could not terminate update process {}".format(e))

    def check_update(self):
        """Main update routine"""
        main_app = 'timerecorder.exe'
        updater_app = 'timerecorderupdater.exe'
        update_check_file = "_updater_checked"

        if sys.platform == 'linux2':
            return False

        self.__logger.info(
            "Checking for updates at {}".format(QtCore.QDateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss")))

        # First get the update file location todo is possible to use github address?
        try:
            with open('//sm.local/public/Temp/Elric/timerecorder/_version_file', 'r') as f:
                version_info = f.readlines()
        except IOError as e:
            self.__logger.debug("Could not access version file")
        else:
            for line in version_info:
                try:
                    appname, appversion = line.strip().split('=')
                except ValueError as e:
                    self.__logger.debug("Incompatible version info: {}".format(e))
                    continue
                # check main app
                if appname == main_app.strip('.exe'):
                    if StrictVersion(appversion) > StrictVersion(__version__):
                        # self.updating_app = appname
                        self.__logger.info("Newer Version of {} Found".format(main_app.strip('.exe')))
                        self.__logger.debug("Running updater..")
                        try:
                            subprocess.Popen([updater_app])
                        except WindowsError as e:
                            self.__logger.error("Error: {}\n".format(e))
                        except Exception as e:
                            self.__logger.error("Error: {}\n".format(e))
                        else:
                            self.__logger.info(
                                "Exiting app in 10s for update: Updating from {} to version {}".format(__version__,
                                                                                                       appversion))
                            # emit signal for closing main app.
                            self.exitmainapp.emit()
                            return True
                    else:
                        self.__logger.info(
                            "No New Version of {} found, current version = {}, latest = {}".format(appname, __version__,
                                                                                                   appversion))
                elif appname == updater_app.strip('.exe'):
                    # self.updating_app = appname
                    if os.path.exists(update_check_file):
                        try:
                            self.__logger.debug(
                                "Starting process at {}".format(QtCore.QDateTime.currentDateTime().toString()))
                            self.__process = subprocess.Popen([updater_app, "-V"], stdin=subprocess.PIPE,
                                                              stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                            version, err = self.__process.communicate()
                            self.__logger.debug(
                                "Ending process at {}".format(QtCore.QDateTime.currentDateTime().toString()))

                            version = version.strip()
                        except WindowsError as e:
                            self.__logger.warning("Could not run file, try an force update")
                            version = '0.0.0'

                        if not version:
                            self.__logger.warning(
                                "Could not get version of {}, trying to  force update".format(updater_app))
                            version = '0.0.0'
                    else:
                        self.__logger.info("Forcing latest updater on first run")
                        version = '0.0.0'

                    u_app = updater.updater(updater_app)
                    if not u_app.is_app_running():
                        if StrictVersion(appversion) > StrictVersion(version):
                            self.__logger.info("Newer Version of Updater Found")

                            if u_app.update():
                                self.__logger.info("Successfully updated {}".format(u_app.app_name))

                                if not os.path.exists("_updater_checked"):
                                    with open(update_check_file, 'a') as f:
                                        os.utime(update_check_file, None)

                        else:
                            self.__logger.debug(
                                "No new version of Updater, current version = {}, Lastest Version = {}".format(
                                    updater.__version__, appversion))


class WaitTimer(QtCore.QObject):
    """Main 'sleep' timer"""
    started = QtCore.pyqtSignal()
    ended = QtCore.pyqtSignal()

    def __init__(self, _time=1, parent=None):
        QtCore.QObject.__init__(self)
        self.__logger = logging.getLogger("{}".format(self.__class__.__name__))
        self.__wait_time = _time
        self.__sleepinguntil = QtCore.QDateTime.currentDateTime().addSecs(_time)
        self.__running = False
        self.__stopped = True
        self.setObjectName("WaitTimer")
        self.__end_signal_ignore = False

    def stop(self):
        self.__stopped = True

    def ignore_end_signal(self, value=True):
        """Set to ignore end signal once"""
        self.__end_signal_ignore = value

    def isrunning(self):
        return self.__running

    @QtCore.pyqtSlot()
    def wait(self):
        """main routine for sleeping and checking state each second"""
        self.__running = True
        self.__stopped = False
        self.started.emit()
        self.__logger.debug("starting sleep")
        # cycle for each second requested
        for i in range(self.__wait_time):
            time.sleep(1)
            if self.__stopped:
                self.__stopped = True
                break
        if self.__end_signal_ignore:
            self.__logger.debug("Ignoring ended signal")
            self.__end_signal_ignore = False
        else:
            self.__logger.debug("Sending ended emit signal")
            self.ended.emit()

        if self.__stopped:
            self.__logger.debug("Ending sleep because of stop")
        else:
            self.__logger.debug("Ending sleep normally")
            self.__stopped = True

        self.__running = False

    def settime(self, wait_time):
        self.__wait_time = wait_time
        self.__sleepinguntil = QtCore.QDateTime.currentDateTime().addSecs(wait_time)

    def remaining_time(self):
        """returns the remaining seconds left in sleep cycle"""
        return QtCore.QDateTime.currentDateTime().secsTo(self.__sleepinguntil)


class AddItemDialog(QtGui.QDialog, addtaskwindow.Ui_AddTaskWindow):
    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        self.setupUi(self)


class LogOptionDialog(QtGui.QDialog, timelogoption.Ui_timeOptionDialog):
    def __init__(self, pmodel=None, amodel=None, dmodel=None, cmodel=None, lmodel=None, parent=None):
        super(self.__class__, self).__init__(parent)
        self.logger = logging.getLogger("{}".format(self.__class__.__name__))
        self.logger.debug("Starting Time Log Option UI")
        self.setupUi(self)

        # todo: make this remember selection rather then set all to first
        self.projectList.setModel(pmodel)
        if pmodel.savedindex() is None:
            index = self.projectList.model().index(0, 0)
        else:
            index = pmodel.savedindex()
        self.projectList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)
        self.projectList.clicked.connect(self._on_change)
        self.addProjectButton.clicked.connect(self.additem)
        self.removeProjectButton.clicked.connect(self.remove_item)

        self.activitiesList.setModel(amodel)
        if amodel.savedindex() is None:
            index = self.activitiesList.model().index(0, 0)
        else:
            index = amodel.savedindex()
        self.activitiesList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)
        self.activitiesList.clicked.connect(self._on_change)
        self.addActivityButton.clicked.connect(self.additem)
        self.removeActivityButton.clicked.connect(self.remove_item)

        self.departmentList.setModel(dmodel)
        if dmodel.savedindex() is None:
            index = self.departmentList.model().index(0, 0)
        else:
            index = dmodel.savedindex()
        self.departmentList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)
        self.departmentList.clicked.connect(self._on_change)
        self.addDepartmentButton.clicked.connect(self.additem)
        self.removeDepartmentButton.clicked.connect(self.remove_item)

        self.classList.setModel(cmodel)
        if cmodel.savedindex() is None:
            index = self.classList.model().index(0, 0)
        else:
            index = cmodel.savedindex()
        self.classList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)
        self.classList.clicked.connect(self._on_change)
        self.addClassButton.clicked.connect(self.additem)
        self.removeClassButton.clicked.connect(self.remove_item)

        self.locationList.setModel(lmodel)
        if cmodel.savedindex() is None:
            index = self.locationList.model().index(0, 0)
        else:
            index = lmodel.savedindex()
        self.locationList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)
        self.locationList.clicked.connect(self._on_change)
        self.addLocationButton.clicked.connect(self.additem)
        self.removeLocationButton.clicked.connect(self.remove_item)

    def _on_change(self):
        sender = self.sender()
        if sender == self.projectList:
            model = self.projectList.model()
            item_list_obj = self.projectList
        elif sender == self.departmentList:
            model = self.departmentList.model()
            item_list_obj = self.departmentList
        elif sender == self.activitiesList:
            model = self.activitiesList.model()
            item_list_obj = self.activitiesList
        elif sender == self.classList:
            model = self.classList.model()
            item_list_obj = self.classList
        elif sender == self.locationList:
            model = self.locationList.model()
            item_list_obj = self.locationList

        model.setindex(item_list_obj.currentIndex())

    def additem(self):
        """dialog box to add item to the task window"""
        sender = self.sender()
        self.logger.debug("Adding item to list, initiated by sender={}".format(sender))

        dialog = AddItemDialog()
        if sender == self.addProjectButton:
            model = self.projectList.model()
            item_list_obj = self.projectList
            dialog.setWindowTitle("Add Project")
        elif sender == self.addDepartmentButton:
            model = self.departmentList.model()
            item_list_obj = self.departmentList
            dialog.setWindowTitle("Add Department")
        elif sender == self.addActivityButton:
            model = self.activitiesList.model()
            item_list_obj = self.activitiesList
            dialog.setWindowTitle("Add Activity")
        elif sender == self.addClassButton:
            model = self.classList.model()
            item_list_obj = self.classList
            dialog.setWindowTitle("Add Class")
        elif sender == self.addLocationButton:
            model = self.locationList.model()
            item_list_obj = self.locationList
            dialog.setWindowTitle("Add Location")

        result = dialog.exec_()
        if result == QtGui.QDialog.Accepted:
            new_item = QtGui.QStandardItem(dialog.lineEdit.text())
            new_item.setDropEnabled(False)
            model.appendRow(new_item)
            item_list_obj.selectionModel().setCurrentIndex(new_item.index(), QtGui.QItemSelectionModel.ClearAndSelect)

    def remove_item(self):
        sender = self.sender()
        self.logger.debug("Removing item from list, initiated by sender={}".format(sender))
        if sender == self.removeProjectButton:
            item_list_obj = self.projectList
        elif sender == self.removeDepartmentButton:
            item_list_obj = self.departmentList
        elif sender == self.removeActivityButton:
            item_list_obj = self.activitiesList
        elif sender == self.removeClassButton:
            item_list_obj = self.classList
        elif sender == self.removeLocationButton:
            item_list_obj = self.locationList

        index = item_list_obj.currentIndex()
        model = index.model().itemFromIndex(index)
        row = model.row()
        item_list_obj.model().removeRow(row)

        # make sure one item is in the list
        if item_list_obj.model().rowCount() == 0:
            new_item = QtGui.QStandardItem("None")
            new_item.setDropEnabled(False)
            item_list_obj.model().appendRow(new_item)
            item_list_obj.selectionModel().setCurrentIndex(new_item.index(), QtGui.QItemSelectionModel.ClearAndSelect)
            return False


class ViewEditTimesheetWindow(QtGui.QDialog, timeentryview.Ui_timeentryview):
    def __init__(self, data_table, parent=None):
        super(self.__class__, self).__init__(parent,
                                             flags=QtCore.Qt.WindowMinimizeButtonHint | QtCore.Qt.WindowMaximizeButtonHint)
        self.logger = logging.getLogger("{}".format(self.__class__.__name__))
        self.logger.debug("Initialising View Edit Timesheet window")
        self.headers = data_table[0]
        self.data_table = data_table
        self.setupUi(self)

        selection_list = ["This Week", "Last Week", "All Time", "Select Range"]
        for item in selection_list:
            self.timeSelectBox.addItem(item)
        self.timeSelectBox.activated.connect(self._timeSelectRange)
        self.dateSelectFrame.hide()

        self.closeButton.clicked.connect(self.close)
        self.exportButton.clicked.connect(self.export_to_csv)

        self.model = QtGui.QStandardItemModel()
        self.model.setHorizontalHeaderLabels(self.headers)
        self.proxyModel = QtGui.QSortFilterProxyModel(self)
        self.proxyModel.setSourceModel(self.model)
        self.tableView.setModel(self.proxyModel)
        self.tableView.setSortingEnabled(True)
        self.tableView.horizontalHeader().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.tableView.verticalHeader().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.proxyModel.dataChanged.connect(self.table_edit)

        self.regexButton.clicked.connect(self.find_text)
        self.saveButton.clicked.connect(self.on_save)
        self.saveButton.setEnabled(False)
        self.connect(parent, QtCore.SIGNAL("savesuccess"), self.save_success)

        self.create_data()
        self.show()
        self._timeSelectRange()

    def on_save(self):
        self.emit(QtCore.SIGNAL("savetable"))

    def save_success(self):
        self.saveButton.setEnabled(False)

    def table_edit(self):
        self.data_table = []
        self.data_table.append(self.headers)
        for irow in xrange(self.model.rowCount()):
            row_data = []
            for icol in xrange(self.model.columnCount()):
                cell = str(self.model.data(self.model.index(irow, icol)).toString())
                row_data.append(cell.replace(" | ", "\n"))
            self.data_table.append(row_data)
        self.emit(QtCore.SIGNAL("tablechange"))
        self.saveButton.setEnabled(True)

    def get_updated_time(self):
        return self.data_table

    def create_data(self):
        for i, row in enumerate(self.data_table):
            if i > 0:
                q_row = []
                for j, item in enumerate(row):
                    q_item = QtGui.QStandardItem(str(self.data_table[i][j]))
                    q_row.append(q_item)
                self.model.appendRow(q_row)
        self.resize_view()

    def find_text(self):
        self.proxyModel.setFilterKeyColumn(-1)
        self.proxyModel.setFilterRegExp(QtCore.QRegExp(str(self.regexEdit.text())))
        self.resize_view()

    def export_to_csv(self):
        date = QtCore.QDate(QtCore.QDate.currentDate())
        if self.timeSelectBox.currentText() == "This Week":
            week, year = date.weekNumber()
            fpostfix = "_week{}-{}".format(week, year)
        elif self.timeSelectBox.currentText() == "Last Week":
            lastweek = date.addDays(-7)
            week, year = lastweek.weekNumber()
            fpostfix = "week{}-{}".format(week, year)
        elif self.timeSelectBox.currentText() == "All Time":
            fpostfix = "_all-time"
        else:
            fpostfix = ''

        fsavedefault = "timesheet" + fpostfix
        filename = QtGui.QFileDialog.getSaveFileName(self, "Save Time Sheet", fsavedefault, "csv (*.csv);;Text (*.txt)")

        if filename:
            csv_table = [self.headers]
            for irow in xrange(self.proxyModel.rowCount()):
                row_data = []
                for icol in xrange(self.proxyModel.columnCount()):
                    cell = str(self.proxyModel.data(self.proxyModel.index(irow, icol)).toString())
                    row_data.append(cell.replace("\n", " | "))
                csv_table.append(row_data)
            with open(filename, "wb") as f:
                writer = csv.writer(f)
                writer.writerows(csv_table)

    def centre_view(self):
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def _update_date_filter(self):
        s_week, s_year = self.dateStartEdit.date().weekNumber()
        e_week, e_year = self.dateEndEdit.date().weekNumber()

        years = e_year - s_year + 1
        if years > 1:
            years = 2

        regyear = ''
        for i in range(years):
            if i > 0:
                regyear += "|"
            regyear += "{}".format(s_year + i)

        regweek = ''
        for i in range(years):
            if i == 0:
                for j in range(s_week, 52):
                    if j > s_week:
                        regweek += "|"
                    regweek += str(j)
                    if years == 1 and j == e_week:
                        break
            else:
                for j in range(1, e_week):
                    regweek += "|{}".format(j)

        regfilter = "{}-{}".format(regweek, regyear)
        self.proxyModel.setFilterKeyColumn(0)
        self.proxyModel.setFilterRegExp(QtCore.QRegExp(regfilter))
        self.resize_view()

    def _timeSelectRange(self):
        if self.timeSelectBox.currentText() == "Select Range":
            if self.dateSelectFrame.isHidden():
                self.dateSelectFrame.show()
            self.dateStartEdit.setDate(QtCore.QDate.currentDate())
            self.dateEndEdit.setDate(QtCore.QDate.currentDate())
            self.dateStartEdit.dateChanged.connect(self._update_date_filter)
            self.dateEndEdit.dateChanged.connect(self._update_date_filter)
        else:
            date = QtCore.QDate(QtCore.QDate.currentDate())
            if not self.dateSelectFrame.isHidden():
                self.dateSelectFrame.hide()
            if self.timeSelectBox.currentText() == "This Week":
                week, year = date.weekNumber()
                filter_str = "{}-{}".format(week, year)
                self.proxyModel.setFilterKeyColumn(0)
                self.proxyModel.setFilterRegExp(QtCore.QRegExp(filter_str))
            elif self.timeSelectBox.currentText() == "Last Week":
                lastweek = date.addDays(-7)
                week, year = lastweek.weekNumber()
                filter_str = "{}-{}".format(week, year)
                self.proxyModel.setFilterKeyColumn(0)
                self.proxyModel.setFilterRegExp(QtCore.QRegExp(filter_str))
            elif self.timeSelectBox.currentText() == "All Time":
                self.proxyModel.setFilterRegExp(QtCore.QRegExp("."))
        self.resize_view()

    def resize_view(self):
        # todo fix so this works properly.
        width = 0
        for col in range(self.model.columnCount()):
            width += self.tableView.sizeHintForColumn(col)

        height = 0
        for row in range(self.model.rowCount()):
            height += self.tableView.sizeHintForRow(row)
        vw = self.width()
        vh = self.height()

        tw = self.tableView.width()
        th = self.tableView.height()

        height = height + vh - th + 30
        width = width + vw - tw + 350

        self.resize(width, height)
        self.centre_view()


class OptionModel(QtGui.QStandardItemModel):
    def __init__(self, parent_name, child_name):
        super(self.__class__, self).__init__()
        self._saved_index = None
        self._parent_name = parent_name
        self._child_name = child_name

    @property
    def parent_name(self):
        return self._parent_name

    @parent_name.setter
    def parent_name(self, name):
        if isinstance(name, str):
            self._parent_name = name
        else:
            raise AttributeError("Expeted <str> got {}".format(type(name)))

    @property
    def child_name(self):
        return self._child_name

    @parent_name.setter
    def child_name(self, name):
        if isinstance(name, str):
            self._child_name = name
        else:
            raise AttributeError("Expeted <str> got {}".format(type(name)))

    def savedindex(self):
        return self._saved_index

    def setindex(self, index):
        if isinstance(index, QtCore.QModelIndex):
            self._saved_index = index

    def validate(self, default='None'):
        """"Validates the model to make sure defaults exits if no data"""
        if not self.rowCount():
            default_item = QtGui.QStandardItem(default)
            default_item.setDragEnabled(False)
            self.appendRow(default_item)
            self.setindex(default_item.index())

    def get_etree_element(self):
        parent_item = Element(self._parent_name)
        for index in range(self.rowCount()):
            child_item = Element(self._child_name)
            model = self.index(index, 0)
            child_item.text = str(model.data().toString())
            if self.savedindex().row() == index:
                child_item.set('selected', 'true')
            parent_item.append(child_item)
        return parent_item

    def load_etree_element(self, tree):
        for i, child in enumerate(tree):
            item = QtGui.QStandardItem(child.text)
            item.setDropEnabled(False)
            self.appendRow(item)
            if child.get('selected', False) == "true" or i == 0:
                self.setindex(item.index())


class TaskItem(QtGui.QStandardItem):
    def __init__(self, task, memolist='None', memo='None'):
        super(self.__class__, self).__init__(task)
        # completer for memo box
        self._memo_completer = CustomQCompleter()
        # self._memo_completer = QtGui.QCompleter()
        self._memo_completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self._memo_completer.setCompletionMode(QtGui.QCompleter.PopupCompletion)
        self._memo_completer.setWrapAround(False)
        self._completer_model = QtGui.QStringListModel()
        self._memo_completer.setModel(self._completer_model)
        # memolist = ["None", "Other", "tseT"]
        self.completer_model = memolist
        self._memotext = memo
        # self._completer_model.setStringList(memolist)
        # self.memoBox.setCompleter(self.memo_completer)

    def add_memo(self, memo):
        stringlist = self._completer_model.stringList()
        if memo not in stringlist:
            stringlist.append(memo)
            self._completer_model.setStringList(stringlist)
            return True
        else:
            return False

    def get_memo_list(self):
        return self._completer_model.stringList()

    @property
    def memotext(self):
        return self._memotext

    @memotext.setter
    def memotext(self, text):
        self._memotext = text

    @property
    def completer_model(self):
        return self._memo_completer

    @completer_model.setter
    def completer_model(self, *args):
        string_list = []
        for arg in args:
            if isinstance(arg, str):
                string_list.append(arg)
            elif isinstance(arg, list):
                string_list += arg
            elif isinstance(arg, (int, float, long)):
                string_list.append(str(arg))
            else:
                raise NotImplementedError
        self._completer_model.setStringList(string_list)


class TaskModel(QtGui.QStandardItemModel):
    def __init__(self):
        super(self.__class__, self).__init__()
        self._saved_index = None
        # self._saved_index = self.index(0,0)

    @property
    def saved_index(self):
        return self._saved_index

    @saved_index.setter
    def saved_index(self, index):
        if isinstance(index, QtCore.QModelIndex):
            self._saved_index = index

    def get_last_item(self):
        try:
            return self.saved_index.model().itemFromIndex(self.saved_index)
        except AttributeError as e:
            # self.saved_index = self.index(0,0)
            return self.itemFromIndex(self.index(0, 0))


class TimeLogItem(QtGui.QStandardItem):
    def __init__(self):
        super(self.__class__, self).__init__()
        self._project = None
        self._location = None
        self._department = None
        self._class = None
        self._activity = None
        self.taskmodel = TaskModel()
        self.setEditable(False)
        self.setDropEnabled(False)

    def get_items(self):
        return [self._project, self._location, self._department, self._class, self._activity]

    def create_default(self):
        self.set_project("None")
        self.set_department("None")
        self.set_activity("None")
        self.set_class("None")
        self.set_location("None")
        self.addTask("Other", "None")

    def setText(self, text=None):
        if text is None:
            text_str = "[{}]-[{}]-[{}]-[{}]-[{}]".format(
                self._project, self._activity, self._location, self._department, self._class)
            super(TimeLogItem, self).setText(text_str)
        else:
            super(TimeLogItem, self).setText(text)

    def addTask(self, task, memolist='None', current_memo='None'):
        new_task = TaskItem(task, memolist, current_memo)

        new_task.setDropEnabled(False)
        self.taskmodel.appendRow(new_task)
        return new_task

    def removeTask(self, index):
        return self.taskmodel.removeRow(index)

    def taskItem(self, index):
        return self.taskmodel.itemFromIndex(index)

    def get_project(self):
        return str(self._project)

    def get_activity(self):
        return str(self._activity)

    def get_class(self):
        return str(self._class)

    def get_location(self):
        return str(self._location)

    def get_department(self):
        return str(self._department)

    def set_project(self, text):
        self._project = text

    def set_activity(self, text):
        self._activity = text

    def set_class(self, text):
        self._class = text

    def set_location(self, text):
        self._location = text

    def set_department(self, text):
        self._department = text

def test_func(self, text):
    print text

class TimeSheetMain(QtGui.QMainWindow, timerecordmain.Ui_MainWindow):
    def __init__(self, logtime=False, parent=None):
        super(self.__class__, self).__init__(parent)
        self.__logger = logging.getLogger("{}".format(self.__class__.__name__))
        self.__logger.debug("Initialising up UI")
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        # make sure window is always on top
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowStaysOnTopHint)

        #bool for logging time entries in separate file.
        self.logtime=logtime

        # monitor whether snoozing
        self.is_snoozing = False

        # monitor whether time tracking
        self.tracking = True

        self.__last_time = QtCore.QDateTime.currentDateTime()

        # models for data
        self.__projectsmodel = OptionModel('projects', 'project')
        self.__departmentmodel = OptionModel('departments', 'department')
        self.__classmodel = OptionModel('classes', 'class')
        self.__locationmodel = OptionModel('locations', 'location')
        self.__activitymodel = OptionModel('activities', 'activity')

        # Se up main timer
        self.__logger.debug("Initialising Main timer")
        self.timer = QtCore.QTimer()
        self.timer.setObjectName("UI_MainTimer")
        self.timer.setParent(self)
        self.timer.timeout.connect(self.update_ui)
        # run every 60 seconds
        self.timer.start(60 * 1000)

        # update check
        self.checkupdatetimer = QtCore.QTimer()
        self.checkupdatetimer.setObjectName("UpdateCheckTimer")
        self.checkupdatetimer.timeout.connect(self.check_update)
        self.checkupdatetimer.setSingleShot(True)

        # timer for closing the application if invoked by updater  or other functions to hard close.
        self.closetimer = QtCore.QTimer()
        self.closetimer.setObjectName("CloseTimer")
        self.closetimer.timeout.connect(self.close)
        self.closetimer.setSingleShot(True)

        # Timer for sleeping system in separate thread
        self.snoozethread = QtCore.QThread()
        self.snoozethread.setParent(self)
        self.wait_timer = WaitTimer(self.reminderTime.value())
        self.snoozethread.started.connect(self.wait_timer.wait)
        self.wait_timer.ended.connect(self.app_show)

        # main set up for buttons and windows
        self.__logger.debug("Setting up main UI features")

        # Set the settings button to open and close settings menu
        self.settingsButton.clicked.connect(self.toggle_settings_menu)

        # snooze button and time entry buttons
        self.snoozeButton.clicked.connect(self.snooze_no_track)
        self.snoozeButton.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.snoozeButton.customContextMenuRequested.connect(self.on_snooze_context)

        # create context menu for snooz button
        self.snoozemenu = QtGui.QMenu(self)
        self.snoozenextdayaction = self.snoozemenu.addAction('Until Tomorrow')
        self.snoozenextdayaction.triggered.connect(self.snooze_to_tomorrow)

        self.dismissButton.clicked.connect(self.dismiss_entry)
        self.completeEntryButton.clicked.connect(self.complete_entry)
        self.completeEntryButton.setFocus()

        # other buttons
        self.addTaskButton.clicked.connect(self.add_task)
        self.deleteTaskButton.clicked.connect(self.remove_task)
        self.exportButton.clicked.connect(self.view_export_timesheet)

        self.addTimeLogOption.clicked.connect(self.add_time_log_option)
        self.removeTimeLogOption.clicked.connect(self.remove_time_log_option)

        # set up connections when input selection changes
        self.logOptionList.clicked.connect(self._on_Project_change)
        self.logOptionList.doubleClicked.connect(self.complete_entry)
        self.taskList.clicked.connect(self._update_memo)

        # update status tips for buttons
        self.update_status_tips()

        # set main view
        self.stackedWidget.setCurrentIndex(0)

        # fixed theme settings
        self.themeSelectBox.addItem('Dark')
        self.themeSelectBox.addItem('Light')
        self.themeSelectBox.setStatusTip("Changes the Task Bar Icon")

        # set up tray and notifications
        """Creates a windows tray icon with a menu"""
        self.__logger.debug("Initialising windows tray icon")
        tray_menu = QtGui.QMenu()
        self.trayQuitAction = tray_menu.addAction("Quit")
        self.trayQuitAction.triggered.connect(self.app_exit)
        self.trayLogTimeAction = tray_menu.addAction("Show")
        self.trayLogTimeAction.triggered.connect(self.app_show)
        self.trayLunchAction = tray_menu.addAction("Snooze/Lunch")
        self.trayLunchAction.triggered.connect(self.snooze_no_track)
        self.traySleepADayAction = tray_menu.addAction("Snooze 1 Day")
        self.traySleepADayAction.triggered.connect(self.snooze_to_tomorrow)

        self.tray = QtGui.QSystemTrayIcon()
        self.tray.setObjectName("SystemTray")
        if self.themeSelectBox.currentText() == 'Dark':
            self.tray.setIcon(QtGui.QIcon('resources/clock-dark-theme.png'))
        else:
            self.tray.setIcon(QtGui.QIcon('resources/clock-light-theme.png'))

        # message notificaiton options
        self.tray.messageClicked.connect(self.app_show)

        self.tray.setContextMenu(tray_menu)
        # if icon clicked on
        self.tray.activated.connect(self.on_tray_activation)
        self.tray.show()
        self.tray.setToolTip("Work Time Tracker")

        self.traynotifytimer = QtCore.QTimer()
        self.traynotifytimer.setObjectName("TrayNotificationTimer")
        self.traynotifytimer.setParent(self)
        self.traynotifytimer.timeout.connect(self.reminder_message)
        self.traynotifytimer.setSingleShot(True)

        # load settings
        try:
            self.load_settings()
        except IOError:
            # If no settings file create an empty one then load it
            self.save_settings()
            self.load_settings()

        self.versionTextLabel.setText("App Version {}".format(__version__))

        # on first start will dismiss entry and sleep for specified reminder time
        if self.tray.isSystemTrayAvailable():
            self.dismiss_entry()
        else:
            self.show()
        # check for update use a call back so main program can start and run.
        self.checkupdatetimer.start(10 * 1000)
        self.update_ui()

    def update_ui(self):
        """Updates fields in UI that provide 'by minute' information
        also does checks on sleep states and function states to confirm if computer has been sleeping
        or if user has initiated a sleep"""
        # print ("sleep thread running = {}".format(self.snoozethread.isRunning()))
        # print self.wait_timer.remaining_time()

        minutes_since = int(self.__last_time.secsTo(QtCore.QDateTime.currentDateTime())) / 60
        self.__logger.debug(
            "Updating UI with Minutes since = {}, tracking = {}, Snoozing = {}".format(minutes_since,
                                                                                       self.tracking,
                                                                                       self.is_snoozing))
        if self.tracking:
            self.__logger.debug("Checking time tracking, not snoozing")
            self.tray.setToolTip("Current interval {}min".format(minutes_since))
            if not self.isHidden():
                self.timeEntryLabel.setText("What have you been working on for the last {} min?".format(minutes_since))
                self.dismissButton.setStatusTip(
                    "Discard last interval of {} min".format(minutes_since))
            # if has been sleeping for more than 6 hours assume user has not been working or if not tracking
            if minutes_since > (6 * 60):
                self.__logger.debug("Dismissing entry as system has been sleeping for longer than 6 hours")
                if self.is_snoozing:
                    self.snoozethread.quit()
                self.dismiss_entry()
            elif minutes_since > int(self.reminderTime.value()):
                # Make sure app is not hidden and display message if not
                self.__logger.debug("Informing user to log time, current minutes {}".format(minutes_since))
                if self.isHidden():
                    self.app_show()
                if not self.traynotifytimer.isActive() and int(self.reminderTime.value() * 2) >= minutes_since:
                    # in msecs (minutes * seconds * msecs)
                    self.traynotifytimer.start(int(self.reminderTime.value()) * 60 * 1000)
        else:
            minutes_left = self.wait_timer.remaining_time() / 60
            self.__logger.debug("Remaining snooze time {}".format(minutes_left))
            # try:
            if minutes_left <= 0:
                # Make sure snooze time terminated if computer sleeping longer than time value.
                self.__logger.debug(
                    "Last Snooze Time = {}, current time = {}, current left = {}min".format(
                        self.snooze_time.toString(),
                        QtCore.QDateTime.currentDateTime().toString(),
                        minutes_left))
                self.__logger.debug("Waking from snooze state: System snoozing longer than snooze time")
                self.wait_timer.stop()
                self.snoozethread.quit()
                self.tracking = True
                # if computer sleeping longer than 6 hours, reset time and dismiss entry
                if minutes_left < -6 * 60:
                    self.__logger.debug("Dismissing entry as system has been snoozing for longer than 6 hours")
                    self.dismiss_entry()
                else:
                    # add snooze time so only tracks time not in snooze while sleeping.
                    self.__last_time = self.__last_time.addSecs(self.wait_timer.remaining_time() * -1)
                    minutes_since = int(self.__last_time.secsTo(QtCore.QDateTime.currentDateTime())) / 60
                    self.tray.setToolTip("Current interval {}min".format(minutes_since))
                    # check if combined time is greater than reminder time and show app to enter time if so.
                    if minutes_since > int(self.reminderTime.value()):
                        self.app_show()
            else:
                self.tray.setToolTip(
                    "Not Tracking for another {}min".format(self.wait_timer.remaining_time() / 60))

                # self.__logger.debug(
                #     "Ending ui update with Minutes since = {}, tracking = {}, Snoozing = {}".format(minutes_since,
                #                                                                                     self.tracking,
                #                                                                                     self.is_snoozing))

    def update_status_tips(self):
        """updates the status tips if settings changed"""
        self.snoozeButton.setStatusTip(
            "Snooze for {} min - Saves Current and goes to lunch munch munch munch...".format(self.snoozeTime.value()))
        self.completeEntryButton.setStatusTip(
            "Records Current Entry and Sleeps for {} min".format(self.reminderTime.value()))

    def _on_Project_change(self, index=QtCore.QModelIndex):
        # save changes to memo box to last task
        try:
            currenttaskitem = self.taskList.model().get_last_item()
        except AttributeError as e:
            self.__logger.debug("No memo, creating default")
            # index = currenttaskitem.index()
            # currentlogitem = index.model().itemFromIndex(index)
            # newtask = currentlogitem.addTask("Other")
            # self.memoBox.setText(newtask.memotext)
            # self.taskList.model().saved_index = newtask.index()
        try:
            currenttaskitem.memotext = self.memoBox.text()
        except AttributeError as e:
            self.__logger.warning("No Memo to save")
        else:
            currenttaskitem.add_memo(self.memoBox.text())
            self.taskList.model().saved_index = self.taskList.currentIndex()

        # update current models and lists in view
        index = self.logOptionList.currentIndex()
        try:
            currentlogitem = index.model().itemFromIndex(index)
        except AttributeError as e:
            self.taskList.model().clear()
            self.memoBox.clear()
            return False

        # set tasklist model from new log option model task model
        self.taskList.setModel(currentlogitem.taskmodel)
        if self.taskList.model().saved_index:
            index = self.taskList.model().saved_index
        else:
            index = self.taskList.model().index(0, 0)
        self.taskList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)

        # self.taskList.model().saved_index = currenttaskitem.index()
        # self._last_index = self.taskList.currentIndex()

        taskitem = self.taskList.model().get_last_item()
        try:
            self.memoBox.setCompleter(taskitem.completer_model)
        except AttributeError as e:
            index = self.logOptionList.currentIndex()
            currentlogitem = index.model().itemFromIndex(index)
            taskitem = currentlogitem.addTask("Other")
            self.memoBox.setText(taskitem.memotext)
            self.taskList.model().saved_index = taskitem.index()
            self.memoBox.setCompleter(taskitem.completer_model)
        finally:
            self.memoBox.setText(taskitem.memotext)
            # self.memoBox.setText(index.data(QtCore.Qt.UserRole).toString())

    def _update_memo(self, index=QtCore.QModelIndex):
        # save changes to memo box to previous task
        currenttaskitem = self.taskList.model().get_last_item()

        currenttaskitem.memotext = self.memoBox.text()
        currenttaskitem.add_memo(self.memoBox.text())

        # set current index as last index
        self.taskList.model().saved_index = self.taskList.currentIndex()

        # set memo box text
        taskitem = self.taskList.model().get_last_item()
        self.memoBox.setCompleter(taskitem.completer_model)
        if not taskitem.memotext:
            taskitem.memotext = 'None'
        self.memoBox.setText(taskitem.memotext)

    def app_hide(self):
        """wrapper for hid function in case other items need to go here"""
        sender = self.sender()
        if sender is None:
            self.__logger.debug("App hide requested by a function")
        else:
            self.__logger.debug("App Hide Requested by {}".format(sender.objectName()))
        self.hide()

    def app_show(self):
        """covers actions required when app is brought out of hiding"""
        sender = self.sender()
        try:
            if sender.objectName():
                self.__logger.debug("App Show Requested by {}".format(sender.objectName()))
            else:
                self.__logger.debug("App Show Requested by Unknown : {}".format(sender))
        except AttributeError:
            self.__logger.debug("App Show Requested by other : {}".format(sender))

        # if opened when not tracking (from snooze) add 1min interval to track. todo decide on size.
        if not self.tracking:
            self.__last_time = QtCore.QDateTime.currentDateTime().addSecs(-1 * 60)
        # Notify out of snooze state and tracking
        self.is_snoozing = False
        self.tracking = True

        # set time entry main windget to show
        self.stackedWidget.setCurrentIndex(0)

        # uncheck settings button if settings were open before minimising or snoozing app
        self.settingsButton.setChecked(False)

        # make enter work to quickly log time
        self.completeEntryButton.setFocus()
        # bring to front and show
        self.__logger.debug("Bringing window to front")
        if self.isHidden():
            self.__logger.debug("App hidden, un hiding app")
            self.show()

        self.__logger.debug("Raising app")
        self.raise_()
        self.__logger.debug("App raised successfully")

        self.__logger.debug("Activating window")
        self.activateWindow()
        self.__logger.debug("Window activated")

        self.update_ui()

        # kill snooze thread last as may take a moment.
        self.snooze_quit_wait()
        # if self.snoozethread.isRunning():
        #     self.wait_timer.ignore_end_signal(True)
        #     self.__logger.debug("Snooze thread still running")
        #     if self.wait_timer.isrunning():
        #         self.__logger.debug("Wait timer is still running, trying to stop")
        #         self.wait_timer.stop()
        #     while self.wait_timer.isrunning():
        #         pass
        #     self.__logger.debug("Wait timer stopped")
        #     self.__logger.debug("Trying to stop snooze thread")
        #     self.snoozethread.quit()
        #     self.__logger.debug("Waiting for snooze thread to quit")
        #     self.snoozethread.wait()
        #     self.__logger.debug("Snooze thread quit")

    def on_tray_activation(self, reason):
        self.__logger.debug("User Clicked On Tray Icon: {}".format(reason))
        """Handles action on tray icon such as clicking or double clicking"""
        if reason == QtGui.QSystemTrayIcon.DoubleClick:
            self.app_show()
        elif reason == QtGui.QSystemTrayIcon.Trigger:
            if self.isVisible():
                self.app_hide()
            else:
                self.app_show()

    def add_time_log_option(self):
        """dialog box to add tasks to the task window"""
        dialog = LogOptionDialog(self.__projectsmodel, self.__activitymodel, self.__departmentmodel, self.__classmodel,
                                 self.__locationmodel, self)
        result = dialog.exec_()
        if result == QtGui.QDialog.Accepted:
            project = TimeLogItem()
            project.set_activity(dialog.activitiesList.currentIndex().data().toString())
            project.set_class(dialog.classList.currentIndex().data().toString())
            project.set_project(dialog.projectList.currentIndex().data().toString())
            project.set_location(dialog.locationList.currentIndex().data().toString())
            project.set_department(dialog.departmentList.currentIndex().data().toString())
            newtask = project.addTask("Other")
            project.setText()
            self.logOptionList.model().appendRow(project)
            self.logOptionList.selectionModel().setCurrentIndex(project.index(),
                                                                QtGui.QItemSelectionModel.ClearAndSelect)

            self.taskList.setModel(project.taskmodel)
            self.taskList.selectionModel().setCurrentIndex(newtask.index(), QtGui.QItemSelectionModel.ClearAndSelect)
            self.memoBox.setText(newtask.memotext)
            self.taskList.model().saved_index = newtask.index()

        if self.logOptionList.model().rowCount() >= 1:
            self.removeTimeLogOption.setDisabled(False)
            self.addTaskButton.setDisabled(False)
            if self.taskList.model().rowCount() >= 1:
                self.deleteTaskButton.setDisabled(False)

    def remove_time_log_option(self):
        if self.logOptionList.model().rowCount() == 1:
            self.removeTimeLogOption.setDisabled(True)
        index = self.logOptionList.currentIndex()
        model = index.model().itemFromIndex(index)
        row = model.row()
        index.model().removeRow(row)
        self._on_Project_change()
        if self.logOptionList.model().rowCount() == 0:
            self.deleteTaskButton.setDisabled(True)
            self.addTaskButton.setDisabled(True)

    def on_snooze_context(self, point):
        self.snoozemenu.exec_(self.snoozeButton.mapToGlobal(point))

    def tray_notification(self, title, message, msecs=(5 * 60 * 1000)):
        """wrapper to show tray notifications"""
        self.__logger.debug("Displaying tray notification")
        self.tray.showMessage(title, message, icon=0, msecs=msecs)

    def reminder_message(self):
        self.tray_notification("Reminder", "Click to log the last {}min".format(
            int(self.__last_time.secsTo(QtCore.QDateTime.currentDateTime())) / 60))

    def view_export_timesheet(self):
        self.__logger.debug("Opening View Edit Timesheet Window")

        try:
            tree = Etree.parse('timesheet.xml')
            root = tree.getroot()
        except IOError:
            self.__logger.warning("Could not find or access timesheet xml file")
            return None

        self.timesheet_table = [["Week", "Project", "Memo", "Department", "Class", "Location", "Activity",
                                 "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]]
        for element in root:
            index = 1
            week_list = list(self.timesheet_table[0])
            # in case of changes to main list, copy and make all items 0, makes it easier to make changes to list.
            for i in range(len(week_list)):
                week_list[i] = "0"

            if element.tag == "week":
                week = element.attrib["week"]
                week_list[self.timesheet_table[0].index("Week")] = week
                time_entries = element.findall('time')
                for t in time_entries:
                    xdate = t.get('date')
                    qdate = QtCore.QDate(int(xdate[:4]), int(xdate[4:6]), int(xdate[6:8]))
                    dayname = qdate.shortDayName(qdate.dayOfWeek())
                    m = float(t.text)
                    if m != 0:
                        hrs = round(m / 60, 2)
                    else:
                        hrs = 0.0
                    week_list[self.timesheet_table[0].index(dayname)] = hrs

                for item in element:
                    if item.tag == 'memo':
                        memo_string = ''
                        for i, task in enumerate(item):
                            if i > 0:
                                memo_string += "\n"
                            memo_string += "{} - {}min".format(task.text, task.get("time", "0"))
                        week_list[self.timesheet_table[0].index("Memo")] = memo_string
                    elif item.tag == 'time':
                        pass
                    else:
                        try:
                            week_list[self.timesheet_table[0].index(item.tag.title())] = item.text
                        except ValueError as e:
                            self.__logger.warning("Warning: {}".format(e))
                self.timesheet_table.append(week_list)
        self.view_timesheet = ViewEditTimesheetWindow(self.timesheet_table, self)
        self.view_timesheet.setParent(self)
        self.connect(self.view_timesheet, QtCore.SIGNAL("savetable"), self.save_table)

    def save_table(self, table=None):
        """Saves a timesheet table to the xml"""
        day_list = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        if table is None:
            table = self.view_timesheet.get_updated_time()
        root = Element('recordedtimes')
        tree = ElementTree(root)

        for i, row in enumerate(table):
            if i == 0:
                continue
            et_week = Element("week")
            et_week.set('week', table[i][table[0].index('Week')])

            et_project = Element('project')
            et_project.text = table[i][table[0].index('Project')]
            et_week.append(et_project)

            et_department = Element('department')
            et_department.text = table[i][table[0].index('Department')]
            et_week.append(et_department)

            et_class = Element('class')
            et_class.text = table[i][table[0].index('Class')]
            et_week.append(et_class)

            et_location = Element('location')
            et_location.text = table[i][table[0].index('Location')]
            et_week.append(et_location)

            et_activity = Element('activity')
            et_activity.text = table[i][table[0].index('Activity')]
            et_week.append(et_activity)

            task_memo = Element('memo')
            tasks = table[i][table[0].index('Memo')].split("\n")
            for t in tasks:
                new_task = Element('task')
                task, time_ = t.split(' - ')
                new_task.text = task
                new_task.set('time', time_.replace('min', ''))
                task_memo.append(new_task)
            et_week.append(task_memo)

            # save times
            week_num, year = table[i][table[0].index('Week')].split('-')
            weekstartdate = datetime.datetime.strptime("{}-{}-0".format(year, week_num),
                                                       "%Y-%W-%w").date() - datetime.timedelta(7)
            for j, day in enumerate(day_list):
                day_time = Element('time')
                day_time.set('units', 'minutes')
                day_time.set('date', weekstartdate.strftime("%Y%m%d"))
                weekstartdate = weekstartdate + datetime.timedelta(days=1)
                try:
                    day_time.text = str(float(table[i][table[0].index(day)]) * 60)
                except ValueError:
                    day_time.text = "0.0"

                et_week.append(day_time)

            root.append(et_week)

        # get pretty time sheet
        prettyxml = toprettyxml(root)

        try:
            with open('timesheet.xml', 'w') as f:
                f.write(prettyxml)
        except IOError as e:
            self.__logger.error("Save Timesheet Failed: {}".format(e))
            self.tray_notification("Time Entry View/Export",
                                   "Error Saving: {}\nFile: {}".format(e.strerror, e.filename), msecs=(3 * 1000))
        else:
            # notify of successful change
            self.tray_notification("Time Entry View/Export", "Changes Saved Successfully", msecs=(3 * 1000))
            self.emit(QtCore.SIGNAL("savesuccess"))

    def add_task(self):
        """dialog box to add tasks to the task window"""
        # update current memo in case of change
        if self.taskList.model().rowCount() > 0:
            currenttaskitem = self.taskList.model().get_last_item()

            currenttaskitem.memotext = self.memoBox.text()
            currenttaskitem.add_memo(self.memoBox.text())
        # model = self.taskList.model().itemFromIndex(self.taskList.currentIndex())
        # model.setData(self.memoBox.text(), QtCore.Qt.UserRole)

        if self.logOptionList.model().rowCount() == 0:
            self.__logger.warning("Can't add task as there are no options to log against")
            return False
        dialog = AddItemDialog()
        dialog.setWindowTitle("Add Task")
        result = dialog.exec_()
        if result == QtGui.QDialog.Accepted:
            new_item = TaskItem(dialog.lineEdit.text())
            new_item.setDropEnabled(False)
            self.taskList.model().appendRow(new_item)
            self.taskList.selectionModel().setCurrentIndex(new_item.index(), QtGui.QItemSelectionModel.ClearAndSelect)
            self.memoBox.setText("None")
            self.taskList.model().saved_index = self.taskList.currentIndex()

        if self.taskList.model().rowCount() >= 1:
            self.deleteTaskButton.setDisabled(False)

    def remove_task(self):
        """handles removing tasks from the task window"""
        if self.taskList.model().rowCount() == 1:
            self.deleteTaskButton.setDisabled(True)
            return

        index = self.taskList.currentIndex()
        model = index.model().itemFromIndex(index)
        row = model.row()
        index.model().removeRow(row)
        self.memoBox.clear()
        self.taskList.model().saved_index = self.taskList.currentIndex()
        if self.taskList.model().rowCount() > 0:
            taskitem = self.taskList.model().get_last_item()
            self.memoBox.setCompleter(taskitem.completer_model)
            self.memoBox.setText(taskitem.memotext)

    def toggle_settings_menu(self):
        """ hides or shows the settings window"""
        if self.settingsButton.isChecked():
            self.stackedWidget.setCurrentIndex(1)
        else:
            self.update_status_tips()
            self.stackedWidget.setCurrentIndex(0)
            self.save_settings()

    def app_exit(self):
        self.__logger.debug("App close requested")
        self.closetimer.start(1000)

    def closeEvent(self, event):
        """handles whether app is minimised or closed on exit"""
        sender = self.sender()
        self.save_settings()
        if not self.minimiseOnExit.isChecked() or sender == self.closetimer:
            self.__logger.debug("Exiting Application, sender = {}".format(sender))
            event.accept()
        elif self.minimiseOnExit.isChecked():
            self.__logger.debug("Minimising app instead of closing, sender = {}".format(sender))
            self.hide()
            event.ignore()

    def complete_entry(self, event=None, snoozetime=None):
        """Accepts current entry and add its to the timesheet"""
        # reset try notify if active
        current_time = int(self.__last_time.secsTo(QtCore.QDateTime.currentDateTime())) / 60
        current_date = str(QtCore.QDate.currentDate().toString("yyyyMMdd")).encode(encoding='UTF-8', errors='strict')
        current_week, current_year = QtCore.QDate.currentDate().weekNumber()
        week_year = "{}-{}".format(current_week, current_year)

        # update memo in case of change
        model = self.taskList.model().itemFromIndex(self.taskList.currentIndex())

        if not model:
            self.tray_notification("No Option to Log", "Please make sure a log option is selected or exists")
            return 1

        if self.memoBox.text() == "":
            model.setData("None", QtCore.Qt.UserRole)
            self.memoBox.setText("None")
        else:
            model.setData(self.memoBox.text(), QtCore.Qt.UserRole)

        try:
            self.traynotifytimer.stop()
        except Exception as e:
            self.__logger.debug("Could not stop notification timer {}".format(e))

        if current_time == 0:
            self.__logger.debug("Not time to log in entry")
            self.dismiss_entry(snoozetime)
            return 1

        if not snoozetime:
            snoozetime = self.reminderTime.value() * 60

        self.__logger.debug("Completing entry and sleeping for {} seconds".format(snoozetime))

        try:
            tree = Etree.parse('timesheet.xml')
            root = tree.getroot()
        except IOError:
            root = Element('recordedtimes')
            tree = ElementTree(root)
        except ElementTree.ParseError as e:
            self.__logger.error("Failed to parse timesheet file: {}".format(e))
            self.__logger.warning("Time sheet file may be corrupt, baking up and creating new time sheet file")
            shutil.copy("timesheet.xml", "timesheet_bak_{}.xml".format(current_date))
            root = Element('recordedtimes')
            tree = ElementTree(root)

        # flag to check if update required
        update = False
        # search tree for existing times to update
        # checks all elements are the same and then updates the time value
        et_weeks = root.findall("./week[@week='{}']".format(week_year))
        model = self.logoptionmodel.item(self.logOptionList.currentIndex().row())
        for element in et_weeks:
            for field in element:
                if field.tag == 'project':
                    if field.text == model.get_project():
                        update = True
                        continue
                    else:
                        update = False
                        break
                if field.tag == 'department':
                    if field.text == model.get_department():
                        update = True
                        continue
                    else:
                        update = False
                        break
                if field.tag == 'class':
                    if field.text == model.get_class():
                        update = True
                        continue
                    else:
                        update = False
                        break
                if field.tag == 'location':
                    if field.text == model.get_location():
                        update = True
                        continue
                    else:
                        update = False
                        break

            if update:
                break

        if update:
            new_time = True
            for time_field in element.findall('time'):
                if time_field.get('date') == current_date:
                    time_field.text = "{:.1f}".format(float(time_field.text) + float(current_time))
                    new_time = False
                    break
            if new_time:
                new_time = Element('time')
                time_ = current_time
                new_time.set('units', 'minutes')
                new_time.set('date', current_date)
                new_time.text = str(time_)
                element.append(new_time)

            memo_field = element.find('memo')
            new_memo = True
            for task in memo_field:
                tmodel = model.taskmodel.index(self.taskList.currentIndex().row(), 0)
                task_text = "{}: {}".format(str(tmodel.data().toString()),
                                            str(tmodel.data(QtCore.Qt.UserRole).toString()))
                if task.text == task_text:
                    time_ = int(task.get('time', default=0)) + current_time
                    task.set('time', str(time_))
                    new_memo = False
                    break
            if new_memo:
                tmodel = model.taskmodel.index(self.taskList.currentIndex().row(), 0)
                task_text = "{}: {}".format(str(tmodel.data().toString()),
                                            str(tmodel.data(QtCore.Qt.UserRole).toString()))
                new_task = Element('task')
                new_task.text = task_text
                new_task.set('time', str(current_time))
                memo_field.append(new_task)
        # if not updated then create new time entry
        else:
            item = Element("week")
            item.set('week', week_year)
            model = self.logoptionmodel.item(self.logOptionList.currentIndex().row())

            current_project = Element('project')
            current_project.text = model.get_project()
            item.append(current_project)

            current_department = Element('department')
            current_department.text = model.get_department()
            item.append(current_department)

            current_class = Element('class')
            current_class.text = model.get_class()
            item.append(current_class)

            current_location = Element('location')
            current_location.text = model.get_location()
            item.append(current_location)

            current_activity = Element('activity')
            current_activity.text = model.get_activity()
            item.append(current_activity)

            task_memo = Element('memo')
            new_task = Element('task')
            tmodel = model.taskmodel.index(self.taskList.currentIndex().row(), 0)

            task_text = "{}: {}".format(str(tmodel.data().toString()),
                                        str(tmodel.data(QtCore.Qt.UserRole).toString()))
            new_task.text = task_text
            new_task.set('time', str(current_time))
            task_memo.append(new_task)
            item.append(task_memo)

            work_time = Element('time')
            work_time.set('units', 'minutes')
            work_time.set('date', current_date)
            work_time.text = str(current_time)
            item.append(work_time)

            root.append(item)

        # save in case of changes to projects or tasks ot memos
        self.save_settings()

        # get pretty time sheet
        prettyxml = toprettyxml(root)

        if self.logtime:
            task_text = "{}: {}".format(str(tmodel.data().toString()),
                                        str(tmodel.data(QtCore.Qt.UserRole).toString()))
            timelogstring ="{};min; logged on ;{}; for; {}; to ;{}_{}_{}_{}_{}_{}\n".format(str(current_time), current_date,
                                                                                     task_text, week_year,
                                                                                     model.get_project(),
                                                                                     model.get_department(),
                                                                                     model.get_class(),
                                                                                     model.get_location(),
                                                                                     model.get_activity())

            with open('timelog.txt', 'a+') as f:
                f.write(timelogstring)

        with open('timesheet.xml', 'w') as f:
            f.write(prettyxml)

        self.__last_time = QtCore.QDateTime.currentDateTime()
        self.hide_sleep(snoozetime)

    def dismiss_entry(self, snoozetime=None):
        """Dismisses current entry and disappears for reminder time"""
        self.__logger.debug("Dismissing last time and sleeping for {} minutes".format(self.reminderTime.value()))
        if not snoozetime:
            snoozetime = self.reminderTime.value() * 60
        self.__last_time = QtCore.QDateTime.currentDateTime()
        self.hide_sleep(snoozetime)

    def snooze_no_track(self):
        """Send applications to sleep for period of time and does not track time
        i.e. going to lunch or during out of work hours"""
        try:
            self.snoozethread.quit()
        except Exception as e:
            self.__logger.debug("Failure to stop snooze thread {}".format(e))
        if not self.tracking:
            self.tray_notification("Snooze", "Already Snoozing", msecs=3000)
            return True
        self.__logger.debug("Snoozing no tracking enabled")
        self.complete_entry(snoozetime=self.snoozeTime.value() * 60)
        self.tracking = False
        self.snooze_time = QtCore.QDateTime.currentDateTime()

    def snooze_quit_wait(self):
        if self.snoozethread.isRunning():
            self.wait_timer.ignore_end_signal(True)
            self.__logger.debug("Snooze thread still running")
            if self.wait_timer.isrunning():
                self.__logger.debug("Wait timer is still running, trying to stop")
                self.wait_timer.stop()
            while self.wait_timer.isrunning():
                pass
            self.__logger.debug("Wait timer stopped")
            self.__logger.debug("Trying to stop snooze thread")
            self.snoozethread.quit()
            self.__logger.debug("Waiting for snooze thread to quit")
            self.snoozethread.wait()
            self.__logger.debug("Snooze thread quit")

    def snooze_to_tomorrow(self):
        try:
            self.snoozethread.quit()
        except Exception as e:
            self.__logger.debug("Failure to stop snooze thread {}".format(e))
        self.__logger.debug("Snoozing no tracking enabled until tomorrow")
        self.tracking = False
        # tomorrow_date = QtCore.QDateTime.currentDateTime().addDays(1).date()
        tomorrow_date = QtCore.QDateTime(QtCore.QDateTime.currentDateTime().addDays(1).date())
        self.snooze_time = QtCore.QDateTime.currentDateTime()
        self.complete_entry(snoozetime=(QtCore.QDateTime.currentDateTime().secsTo(tomorrow_date)))

    def hide_sleep(self, sleep_seconds):
        """Hides application for given number of seconds"""
        self.__logger.debug("Going to sleep for {} seconds".format(sleep_seconds))

        # reset notify
        try:
            self.traynotifytimer.stop()
        except Exception as e:
            self.__logger.debug("Failed to Kill Notification Timer {}".format(e))

        self.app_hide()
        # confirm thread is stopped before starting new thread
        self.snooze_quit_wait()
        # if self.snoozethread.isRunning():
        #     self.wait_timer.ignore_end_signal(True)
        #     self.__logger.debug("Snooze thread still running")
        #     if self.wait_timer.isrunning():
        #         self.__logger.debug("Wait timer is still running, trying to stop")
        #         self.wait_timer.stop()
        #     while self.wait_timer.isrunning():
        #         pass
        #     self.__logger.debug("Wait timer stopped")
        #     self.__logger.debug("Trying to stop snooze thread")
        #     self.snoozethread.quit()
        #     self.__logger.debug("Waiting for snooze thread to quit")
        #     self.snoozethread.wait()
        #     self.__logger.debug("Snooze thread quit")

        self.is_snoozing = True
        self.wait_timer.settime(sleep_seconds)
        self.wait_timer.moveToThread(self.snoozethread)
        self.snoozethread.start()

        # self.snoozethread = SnoozeThread(sleep_seconds)
        # self.snoozethread.setParent(self)
        # self.connect(self.snoozethread, QtCore.SIGNAL("show"), self.app_show)
        # self.connect(self.snoozethread, QtCore.SIGNAL("hide"), self.app_hide)
        # self.snoozethread.start()

        self.update_ui()

    def load_settings(self):
        """Run on start up to load settings from the settings file"""
        self.__logger.debug("Loading Settings")
        try:
            tree = Etree.parse('settings.xml')
            root = tree.getroot()
        except IOError:
            # create root
            root = Element('settings')
            tree = ElementTree(root)

        self.logoptionmodel = QtGui.QStandardItemModel()
        self.logOptionList.setModel(self.logoptionmodel)

        for child in root:
            if child.tag == 'reminder_interval':
                self.reminderTime.setValue(int(child.text))
            elif child.tag == 'snooze_interval':
                self.snoozeTime.setValue(int(child.text))
            elif child.tag == 'MinimiseOnExit':
                if (child.text == 'False') or (child.text == '0'):
                    self.minimiseOnExit.setChecked(False)
                else:
                    self.minimiseOnExit.setChecked(True)
            elif child.tag == self.__projectsmodel.parent_name:
                self.__projectsmodel.load_etree_element(child)
                # for i, project in enumerate(child):
                #     item = QtGui.QStandardItem(project.text)
                #     item.setDropEnabled(False)
                #     self.__projectsmodel.appendRow(item)
                #     if project.get('selected', False) == "true" or i == 0:
                #         self.__projectsmodel.setindex(item.index())
            elif child.tag == self.__departmentmodel.parent_name:
                self.__departmentmodel.load_etree_element(child)
                # for i, dep in enumerate(child):
                #     item = QtGui.QStandardItem(dep.text)
                #     item.setDropEnabled(False)
                #     self.__departmentmodel.appendRow(item)
                #     if dep.get('selected', False) == "true" or i == 0:
                #         self.__departmentmodel.setindex(item.index())
            elif child.tag == self.__classmodel.parent_name:
                self.__classmodel.load_etree_element(child)
                # for i, cl in enumerate(child):
                #     item = QtGui.QStandardItem(cl.text)
                #     item.setDropEnabled(False)
                #     self.__classmodel.appendRow(item)
                #     if cl.get('selected', False) == "true" or i == 0:
                #         self.__classmodel.setindex(item.index())
            elif child.tag == self.__locationmodel.parent_name:
                self.__locationmodel.load_etree_element(child)
                # for i, loc in enumerate(child):
                #     item = QtGui.QStandardItem(loc.text)
                #     item.setDropEnabled(False)
                #     self.__locationmodel.appendRow(item)
                #     if loc.get('selected', False) == "true" or i == 0:
                #         self.__locationmodel.setindex(item.index())
            elif child.tag == self.__activitymodel.parent_name:
                self.__activitymodel.load_etree_element(child)
                # for i, act in enumerate(child):
                #     item = QtGui.QStandardItem(act.text)
                #     item.setDropEnabled(False)
                #     self.__activitymodel.appendRow(item)
                #     if act.get('selected', False) == "true" or i == 0:
                #         self.__activitymodel.setindex(item.index())
            elif child.tag == 'logoptions':
                for opts in child:
                    project = TimeLogItem()
                    for opt in opts:
                        if opt.tag == "logactivity":
                            project.set_activity(opt.text)
                        elif opt.tag == "logclass":
                            project.set_class(opt.text)
                        elif opt.tag == "logproject":
                            project.set_project(opt.text)
                        elif opt.tag == "loglocation":
                            project.set_location(opt.text)
                        elif opt.tag == "logdepartment":
                            project.set_department(opt.text)
                        elif opt.tag == "logtasks":
                            for task_ in opt:
                                memo_list = []
                                for memo in task_:
                                    if memo.text:
                                        memo_list.append(memo.text)
                                # next bit is just for backwards compatibility
                                if task_.text:
                                    if task_.text.strip():
                                        new_task = project.addTask(task_.text, task_.get('memo', 'None'),
                                                                   task_.get('memo', 'None'))
                                    else:
                                        new_task = project.addTask(task_.get('name', 'Other'), memo_list,
                                                                   task_.get('memo', 'None'))
                                else:
                                    new_task = project.addTask(task_.get('name', 'Other'), memo_list,
                                                               task_.get('memo', 'None'))
                                if task_.get('selected', False) == "true":
                                    project.taskmodel.saved_index = new_task.index()

                    project.setText()
                    self.logoptionmodel.appendRow(project)
                    if opts.get('selected', False) == "true":
                        self.logOptionList.selectionModel().setCurrentIndex(project.index(),
                                                                            QtGui.QItemSelectionModel.ClearAndSelect)
            elif child.tag == 'theme':
                self.themeSelectBox.setCurrentIndex(int(child.attrib['index']))

        # make sure mode is valid-creates defaults if not
        self.__projectsmodel.validate()
        self.__activitymodel.validate()
        self.__departmentmodel.validate()
        self.__classmodel.validate()
        self.__locationmodel.validate()

        # if self.__projectsmodel.rowCount() == 0:
        #     item = QtGui.QStandardItem("None")
        #     item.setDropEnabled(False)
        #     self.__projectsmodel.appendRow(item)
        #     self.__projectsmodel.setindex(item.index())
        # if self.__activitymodel.rowCount() == 0:
        #     item = QtGui.QStandardItem("None")
        #     item.setDropEnabled(False)
        #     self.__activitymodel.appendRow(item)
        #     self.__activitymodel.setindex(item.index())
        # if self.__departmentmodel.rowCount() == 0:
        #     item = QtGui.QStandardItem("None")
        #     item.setDropEnabled(False)
        #     self.__departmentmodel.appendRow(item)
        #     self.__departmentmodel.setindex(item.index())
        # if self.__classmodel.rowCount() == 0:
        #     item = QtGui.QStandardItem("None")
        #     item.setDropEnabled(False)
        #     self.__classmodel.appendRow(item)
        #     self.__classmodel.setindex(item.index())
        # if self.__locationmodel.rowCount() == 0:
        #     item = QtGui.QStandardItem("None")
        #     item.setDropEnabled(False)
        #     self.__locationmodel.appendRow(item)
        #     self.__locationmodel.setindex(item.index())

        try:
            index = self.logOptionList.currentIndex()
            model = index.model().itemFromIndex(index)
            if not model:
                raise AttributeError
        except AttributeError as e:
            self.__logger.warning("Could not load current selection: {}".format(e))
            self.__logger.info("Creating default items")
            item = TimeLogItem()
            item.create_default()
            item.setText()
            self.logoptionmodel.appendRow(item)
            self.logOptionList.selectionModel().setCurrentIndex(item.index(),
                                                                QtGui.QItemSelectionModel.ClearAndSelect)
            index = self.logOptionList.currentIndex()
            model = index.model().itemFromIndex(index)
            self.taskList.setModel(model.taskmodel)
            index = self.taskList.model().index(0, 0)
            self.taskList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)
            self.taskList.model().saved_index = self.taskList.currentIndex()
            self.memoBox.setText(index.data(QtCore.Qt.UserRole).toString())
        else:
            self.taskList.setModel(model.taskmodel)
            if self.taskList.model().saved_index:
                index = self.taskList.model().saved_index
            else:
                index = self.taskList.model().index(0, 0)
            self.taskList.selectionModel().setCurrentIndex(index, QtGui.QItemSelectionModel.ClearAndSelect)
            self.taskList.model().saved_index = self.taskList.currentIndex()
            taskitem = self.taskList.model().itemFromIndex(self.taskList.currentIndex())
            self.memoBox.setCompleter(taskitem.completer_model)
            self.memoBox.setText(taskitem.memotext)

        # connect signals
        # self.logOptionList.selectionModel().selectionChanged.connect(self._on_Project_change)
        # self.taskList.selectionModel().selectionChanged.connect(self._update_memo)

        self.update_status_tips()

    def save_settings(self):
        """saves all the current app settings to an xml file"""
        # change theme items
        # change task bar theme
        self.__logger.debug("Saving Settings")
        if self.themeSelectBox.currentText() == 'Dark':
            self.tray.setIcon(QtGui.QIcon('resources/clock-dark-theme.png'))
        else:
            self.tray.setIcon(QtGui.QIcon('resources/clock-light-theme.png'))
        # create root
        root = Element('settings')
        tree = ElementTree(root)

        # Save Reminder Time
        reminder_interval = Element('reminder_interval')
        reminder_interval.set('unit', 'minutes')
        reminder_interval.text = str(self.reminderTime.value())
        root.append(reminder_interval)

        # save snooze time
        snooze_interval = Element('snooze_interval')
        snooze_interval.set('unit', 'minutes')
        snooze_interval.text = str(self.snoozeTime.value())
        root.append(snooze_interval)

        # save minmise on exit
        item = Element('MinimiseOnExit')
        item.set('type', 'bool')
        item.text = '1' if self.minimiseOnExit.isChecked() else '0'
        item.text = str(self.minimiseOnExit.isChecked())
        root.append(item)

        # save Projects
        root.append(self.__projectsmodel.get_etree_element())
        # pitem = Element("projects")
        # for index in range(self.__projectsmodel.rowCount()):
        #     sitem = Element("project")
        #     model = self.__projectsmodel.index(index, 0)
        #     sitem.text = str(model.data().toString())
        #     if self.__projectsmodel.savedindex().row() == index:
        #         sitem.set('selected', 'true')
        #     pitem.append(sitem)
        # root.append(pitem)

        # save Departments
        root.append(self.__departmentmodel.get_etree_element())
        # pitem = Element("departments")
        # for index in range(self.__departmentmodel.rowCount()):
        #     sitem = Element("department")
        #     model = self.__departmentmodel.index(index, 0)
        #     sitem.text = str(model.data().toString())
        #     if self.__departmentmodel.savedindex().row() == index:
        #         sitem.set('selected', 'true')
        #     pitem.append(sitem)
        # root.append(pitem)

        # save classes
        root.append(self.__classmodel.get_etree_element())
        # pitem = Element("classes")
        # for index in range(self.__classmodel.rowCount()):
        #     sitem = Element("class")
        #     model = self.__classmodel.index(index, 0)
        #     sitem.text = str(model.data().toString())
        #     if self.__classmodel.savedindex().row() == index:
        #         sitem.set('selected', 'true')
        #     pitem.append(sitem)
        # root.append(pitem)

        # save Locations
        root.append(self.__locationmodel.get_etree_element())
        # pitem = Element("locations")
        # for index in range(self.__locationmodel.rowCount()):
        #     sitem = Element("location")
        #     model = self.__locationmodel.index(index, 0)
        #     sitem.text = str(model.data().toString())
        #     if self.__locationmodel.savedindex().row() == index:
        #         sitem.set('selected', 'true')
        #     pitem.append(sitem)
        # root.append(pitem)

        # save Activities
        root.append(self.__activitymodel.get_etree_element())
        # pitem = Element("activities")
        # for index in range(self.__activitymodel.rowCount()):
        #     sitem = Element("activity")
        #     model = self.__activitymodel.index(index, 0)
        #     sitem.text = str(model.data().toString())
        #     if self.__activitymodel.savedindex().row() == index:
        #         sitem.set('selected', 'true')
        #     pitem.append(sitem)
        # root.append(pitem)

        # save log options
        pitem = Element("logoptions")
        for index in range(self.logoptionmodel.rowCount()):
            model = self.logoptionmodel.item(index)
            sitem = Element("logoption")
            if self.logOptionList.currentIndex().row() == index:
                sitem.set('selected', 'true')
            prj_item = Element("logproject")
            prj_item.text = model.get_project()
            sitem.append(prj_item)
            act_item = Element("logactivity")
            act_item.text = model.get_activity()
            sitem.append(act_item)
            dep_item = Element("logdepartment")
            dep_item.text = model.get_department()
            sitem.append(dep_item)
            cls_item = Element("logclass")
            cls_item.text = model.get_class()
            sitem.append(cls_item)
            loc_item = Element("loglocation")
            loc_item.text = model.get_location()
            sitem.append(loc_item)
            task_items = Element("logtasks")
            for row, tindex in enumerate(range(model.taskmodel.rowCount())):
                task_item = Element("logtask")
                if self.taskList.currentIndex().row() == row:
                    task_item.set('selected', 'true')
                tmodel = model.taskmodel.index(tindex, 0)
                task_item.set('name', str(tmodel.data().toString()))
                # task_item.text = str(tmodel.data().toString())
                # memo_item = Element("memos")
                titem = model.taskmodel.itemFromIndex(tmodel)
                for memo in titem.get_memo_list():
                    new_memo = Element("memo")
                    new_memo.text = str(memo)
                    task_item.append(new_memo)
                # task_item.append(memo_item)
                task_item.set('memo', str(titem.memotext))
                task_items.append(task_item)
            sitem.append(task_items)
            pitem.append(sitem)
        root.append(pitem)

        # theme
        theme = Element('theme')
        theme.text = str(self.themeSelectBox.currentText())
        theme.set('index', str(self.themeSelectBox.currentIndex()))
        root.append(theme)

        # make pretty
        prettyxml = toprettyxml(root)

        with open('settings.xml', 'w') as f:
            f.write(prettyxml)

    def check_update(self):
        self.updatethread = UpdateThread()
        self.updatethread.exitmainapp.connect(self.app_exit)
        # self.connect(self.updatethread, QtCore.SIGNAL("close"), self.close)
        self.updatethread.setObjectName("UpdateThread")
        self.updatethread.start()

        self.checkupdatetimer.start(1 * 60 * 60 * 1000)


def main(args):
    # setup logger
    system_config_file__name = "sysconfig.txt"
    valid_log_cases = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET']
    logtime = False
    if os.path.exists(system_config_file__name):
        with open(system_config_file__name) as scfg:
            system_config = scfg.readlines()
            for line in system_config:
                setting, value = line.strip().split('=')
                if setting == 'loglevel':
                    loglevel=value
                    if loglevel not in valid_log_cases:
                        loglevel = 'INFO'
                elif setting == 'logtime':
                    if value.lower() in ['1', 'true']:
                        logtime = True
    else:
        with open(system_config_file__name, 'w') as scfg:
            scfg.write('loglevel=INFO')
        loglevel = "INFO"
    logfiledir = "runlogs"
    logfilename = "timerecorder_run.log"
    logfilepath = os.path.join(logfiledir, logfilename)
    if not os.path.exists(logfiledir):
        os.makedirs(logfiledir)

    logging.basicConfig(filename=logfilepath, level=loglevel,
                        format='%(asctime)s: %(name)s::%(funcName)s: %(levelname)s: %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    logger = logging.getLogger(__name__)

    logger.debug("Starting Main Program")
    # check if app is already running
    if not loglevel == 'DEBUG':
        checkapp = updater.updater('timerecorder.exe')
        if checkapp.is_app_running():
            logger.error("App already running")
            sys.exit(0)
    # catch unhandled exceptions
    sys.excepthook = unhandled_exception

    app = QtGui.QApplication(sys.argv)
    form = TimeSheetMain(logtime)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main(sys.argv)
