import os
import sys
import shutil
import subprocess
import time
import logging
import win32ui
from datetime import datetime

__version__ = "1.1.0"

def unhandled_exception(exc_type, exc_value, exc_tb):
    logger = logging.getLogger(sys._getframe().f_code.co_name)
    logger.error("Unhandled Exception", exc_info=(exc_type, exc_value, exc_tb))
    sys.exit(1)

class updater(object):
    """
        Simple Class to copy an app from a source directory
        Has a useful function to check whether the app is running
    """
    def __init__(self, app, source_dir=None, temp_dir='temp'):
        self.logger = logging.getLogger("{}".format(self.__class__.__name__))
        self.app_full_name = app
        self.app_name = app.strip('.exe')
        self.source_dir = source_dir
        self.temp_dir = temp_dir

        if not self.source_dir:
            self.source_dir = '//sm.local/public/Temp/Elric/timerecorder/'

        self.path_to_app = self.source_dir + self.app_full_name

    def update(self, tries=3, timeout=180):
        """ main method of class to copy the app to a temp directory and if successful replace the app to update"""
        # wait for main program to close
        app_running = True
        self.logger.debug("waiting for {} to close".format(self.app_name))
        for i in range(tries):
            if self.is_app_running():
                self.logger.debug("App Is running")
                time.sleep(int(timeout/tries))
            else:
                app_running = False
                break

        if app_running:
            self.logger.error("Timed out waiting for app to close")
            return False

        success = False
        if not os.path.isdir(self.temp_dir):
            self.logger.debug("Creating Temp Directory")
            os.mkdir(self.temp_dir)
        try:
            shutil.copy(self.path_to_app, self.temp_dir)
        except IOError as e:
            self.logger.error("Could not download/copy update file")
        else:
            for i in range(10):
                try:
                    self.logger.info("Copying file try #{}".format(i))
                    shutil.copy(os.path.join(self.temp_dir, self.app_full_name), '.')
                except IOError as e:
                    self.logger.error("Update error {}".format(e))
                else:
                    self.logger.info("Update Successful")
                    success = True
                    break
                time.sleep(10)

        try:
            shutil.rmtree(self.temp_dir)
        except IOError as e:
            self.logger.error("Failed to delete temp folder{}".format(e))

        return success

    def is_app_running(self):
        """Method to check if app is running"""
        try:
            win32ui.FindWindow(None, self.app_name)
            return True
        except win32ui.error as e:
            return False


def main(args):
    system_config_file__name = "sysconfig.txt"
    valid_log_cases = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET']
    if os.path.exists(system_config_file__name):
        with open(system_config_file__name) as scfg:
            loglevel = scfg.readline().split("=")[-1].upper()
            if loglevel not in valid_log_cases:
                loglevel = 'INFO'
    else:
        with open(system_config_file__name, 'w') as scfg:
            scfg.write('loglevel=INFO')
        loglevel = "INFO"

    logfiledir = "runlogs"
    # logfilename = "{}_updater.log".format(datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))
    logfilename = "updater.log"
    logfilepath = os.path.join(logfiledir, logfilename)
    if not os.path.exists(logfiledir):
        os.makedirs(logfiledir)

    logging.basicConfig(filename=logfilepath, level=loglevel,
                        format='%(asctime)s: %(name)s: %(levelname)s: %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
    logger = logging.getLogger(__name__)

    if '-V' in args or '--version' in args:
        print __version__
        return 0

    logger.debug("Starting Updater")
    # catch unhandled exceptions
    sys.excepthook = unhandled_exception

    app = updater('timerecorder.exe')
    success = app.update()

    if not app.is_app_running():
        logger.info("App is not running, opening app")
        try:
            subprocess.Popen([app.app_full_name])
        except WindowsError as e:
            logger.error("Failed to open app, Windows Error: {}".format(e))
    else:
        logger.error("{} is already running".format(app.app_name))

    logger.info("Exiting Updater with Success = {}".format(success))
    sys.exit(0)

if __name__ == '__main__':
    main(sys.argv[1:])
