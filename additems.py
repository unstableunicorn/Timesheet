# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'additems.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_addItemsForm(object):
    def setupUi(self, addItemsForm):
        addItemsForm.setObjectName(_fromUtf8("addItemsForm"))
        addItemsForm.resize(429, 328)
        self.gridLayout = QtGui.QGridLayout(addItemsForm)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setContentsMargins(0, 0, -1, -1)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.addButton = QtGui.QPushButton(addItemsForm)
        self.addButton.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("resources/arrow-right.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addButton.setIcon(icon)
        self.addButton.setIconSize(QtCore.QSize(20, 20))
        self.addButton.setFlat(True)
        self.addButton.setObjectName(_fromUtf8("addButton"))
        self.verticalLayout.addWidget(self.addButton)
        self.removeButton = QtGui.QPushButton(addItemsForm)
        self.removeButton.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("resources/arrow-left.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.removeButton.setIcon(icon1)
        self.removeButton.setIconSize(QtCore.QSize(20, 20))
        self.removeButton.setFlat(True)
        self.removeButton.setObjectName(_fromUtf8("removeButton"))
        self.verticalLayout.addWidget(self.removeButton)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.gridLayout.addLayout(self.verticalLayout, 1, 4, 1, 1)
        self.itemsAvilableList = QtGui.QListWidget(addItemsForm)
        self.itemsAvilableList.setObjectName(_fromUtf8("itemsAvilableList"))
        self.gridLayout.addWidget(self.itemsAvilableList, 1, 0, 1, 1)
        self.itemsShownList = QtGui.QListWidget(addItemsForm)
        self.itemsShownList.setObjectName(_fromUtf8("itemsShownList"))
        self.gridLayout.addWidget(self.itemsShownList, 1, 5, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(addItemsForm)
        self.buttonBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout.addWidget(self.buttonBox, 4, 5, 1, 1, QtCore.Qt.AlignRight|QtCore.Qt.AlignBottom)
        self.label = QtGui.QLabel(addItemsForm)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtGui.QLabel(addItemsForm)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 5, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(-1, 5, -1, -1)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.addTaskButton = QtGui.QPushButton(addItemsForm)
        self.addTaskButton.setMinimumSize(QtCore.QSize(20, 20))
        self.addTaskButton.setMaximumSize(QtCore.QSize(20, 20))
        self.addTaskButton.setText(_fromUtf8(""))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8("resources/plus.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addTaskButton.setIcon(icon2)
        self.addTaskButton.setIconSize(QtCore.QSize(20, 20))
        self.addTaskButton.setFlat(True)
        self.addTaskButton.setObjectName(_fromUtf8("addTaskButton"))
        self.horizontalLayout.addWidget(self.addTaskButton)
        self.deleteTaskButton = QtGui.QPushButton(addItemsForm)
        self.deleteTaskButton.setMinimumSize(QtCore.QSize(20, 20))
        self.deleteTaskButton.setMaximumSize(QtCore.QSize(20, 20))
        self.deleteTaskButton.setText(_fromUtf8(""))
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8("resources/minus.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.deleteTaskButton.setIcon(icon3)
        self.deleteTaskButton.setIconSize(QtCore.QSize(20, 20))
        self.deleteTaskButton.setFlat(True)
        self.deleteTaskButton.setObjectName(_fromUtf8("deleteTaskButton"))
        self.horizontalLayout.addWidget(self.deleteTaskButton)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.gridLayout.addLayout(self.horizontalLayout, 4, 0, 1, 1)

        self.retranslateUi(addItemsForm)
        QtCore.QMetaObject.connectSlotsByName(addItemsForm)

    def retranslateUi(self, addItemsForm):
        addItemsForm.setWindowTitle(_translate("addItemsForm", "Add Items Form", None))
        self.label.setText(_translate("addItemsForm", "Items Available", None))
        self.label_2.setText(_translate("addItemsForm", "Items to Show", None))
        self.addTaskButton.setStatusTip(_translate("addItemsForm", "Adds A New Task and Selects It", None))
        self.deleteTaskButton.setStatusTip(_translate("addItemsForm", "Removes Currently Selected Task", None))

