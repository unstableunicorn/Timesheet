rem set PYINSTALLER_PATH="C:\PyInstaller-3.2"
set PYINSTALLER_PATH="..\pyinstaller-develop"
set PYPATH="C:\Anaconda2\python.exe"
rem %PYPATH% %PYINSTALLER_PATH%\pyinstaller.py --onefile --log-level=DEBUG --noconsole --noupx --distpath=timerecorder --name=timerecorder --icon=resources\clock-light-theme.ico timesheet.py
%PYPATH% %PYINSTALLER_PATH%\pyinstaller.py timerecorder.spec --onefile --distpath=timerecorder_release_build
%PYPATH% %PYINSTALLER_PATH%\pyinstaller.py timerecorder-debug.spec --onefile --log-level=DEBUG --distpath=timerecorder_debug_build
%PYPATH% %PYINSTALLER_PATH%\pyinstaller.py timerecorderupdater.spec --onefile --distpath=timerecorder_release_build
%PYPATH% %PYINSTALLER_PATH%\pyinstaller.py timerecorderupdater-debug.spec --onefile --log-level=DEBUG --distpath=timerecorder_debug_build

xcopy /E /Y resources\* timerecorder_debug_build\resources\
xcopy /E /Y resources\* timerecorder_release_build\resources\