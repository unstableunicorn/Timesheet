# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'addtaskwindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_AddTaskWindow(object):
    def setupUi(self, AddTaskWindow):
        AddTaskWindow.setObjectName(_fromUtf8("AddTaskWindow"))
        AddTaskWindow.resize(260, 106)
        self.gridLayout = QtGui.QGridLayout(AddTaskWindow)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.lineEdit = QtGui.QLineEdit(AddTaskWindow)
        self.lineEdit.setWhatsThis(_fromUtf8(""))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout.addWidget(self.lineEdit, 0, 0, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(AddTaskWindow)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 1)

        self.retranslateUi(AddTaskWindow)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), AddTaskWindow.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), AddTaskWindow.reject)
        QtCore.QMetaObject.connectSlotsByName(AddTaskWindow)

    def retranslateUi(self, AddTaskWindow):
        AddTaskWindow.setWindowTitle(_translate("AddTaskWindow", "Add Task", None))
        AddTaskWindow.setWhatsThis(_translate("AddTaskWindow", "Enter the name of a task to add it to the list", None))

